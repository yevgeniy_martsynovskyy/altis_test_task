import React from 'react';
import ImageTypeSwitcher from './ImageTypeSwitcher';
import ImageSpaceSwitcher from './ImageSpaceSwitcher';

export default () => {
    return (
        <div className="control-panel page__element">
            <ImageTypeSwitcher />
            <ImageSpaceSwitcher />
        </div>
    )
}
