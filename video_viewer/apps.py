from django.apps import AppConfig


class VideoViewerConfig(AppConfig):
    name = 'video_viewer'
