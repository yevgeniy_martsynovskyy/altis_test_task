from django.urls import path

from . import views

app_name = 'video_viewer_api'

urlpatterns = [
    path('test/', views.test, name='test'),
    path('get_videos_list/', views.get_videos_list, name='get_videos_list'),
    path('get_video_info/', views.get_video_info, name='get_video_info'),
    path('get_image/', views.get_image, name='get_image')
]
