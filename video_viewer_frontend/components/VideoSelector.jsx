import React from 'react';

import { useSelector, useDispatch } from 'react-redux';
import { fetchVideoInfo } from '../redux/actions';

export default () => {

    const backend_api = useSelector(state => state.app.backend_api);
    const videos = useSelector(state => state.player.videosList);

    const dispatch = useDispatch();

    const changeHandler = event => {
        event.preventDefault();

        let url = backend_api + 'get_video_info?filename=' + event.target.value;
        url = encodeURI(url);

        dispatch(fetchVideoInfo(url))
    }

    return (
        <select className="video-selector" onChange={changeHandler}>
            {videos.map(item => {
                return <option key={item} value={item}>{item}</option>
            })}
        </select>
    )
}
