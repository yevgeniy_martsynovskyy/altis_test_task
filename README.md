### Структура проекта:

+ `django_backend` -- файлы относящиеся к настройкам Django в целом. Особенно важны `settings.py` и `urls.py`
+ `video_utils` -- основное место где происходит обработка видео. Набор всех нужных функций лежит в `image_extractor.py`
+ `video_viewer` -- нужен, чтобы хранить фронтенд болванку, куда вставляется React приложение. Единственный нужный файл `templates/video-viewer/index.html`
+ `video_viewer_api` -- серверный API, который обрабатывает запросы от фронта. Самое важное находится в `views.py`
+ `video_viewer_frontend` -- исходный код React приложения. Из файликов в этой папке Webpack собирает конечные бандлы:
    * `components` -- JSX компоненты приложения
    * `redux` -- все что относится к Redux: reducers, actions, types 
    * `styles` -- CSS стили приложения. Старался следовать БЭМ методологии и соответствующему наименованию классов
+ `.dockerignore`
+ `.gitignore`
+ `backend_container_init.sh` -- shell скрипт, чтобы выполнить подготовку к запуску Django в Docker, создать пустую базу данных (если отсутствует), собрать все статические файлы в одну директорию, пробрасывать SIGTERM от Докера в UWSGI
+ `docker-compose.yml` -- файл Docker Compose, чтобы запускать два контенера: nginx и uwsgi+django
+ `Dockerfile` -- нужен для создания Докер-образа с Джанго-приложением и Реакт-бандлами
+ `manage.py` -- автоматически создается при создании Джанго проекта. Позволяет запускать dev сервер
+ `reuqirements.txt` -- все что нужно питону для работы приложения
+ `webpack.config.js` -- конфиг Вебпака
+ `package.json` -- список npm пакетов

### Подготовка к запуску:
+ Создать пустые директории `databases` и `video_files`
+ В `video-files` положить MKV файлы, которые можно будет посмотреть через веб-интерфейс
+ `npm install`
+ Собрать докер-образ командой `docker build -t shaihulud1992/altis_test_task .`

### Запуск в контейнерах:
+ `docker-compose up -d` (не работают обращения к OpenGL)

### Запуск в режиме разработки:
+ В одном терминальном окне `python3 manage.py runserver`
+ В другом `npm run watch`

### Куда смотреть:
+ `host:port/video_viewer/`
