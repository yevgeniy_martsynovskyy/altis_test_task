import React, { useEffect } from 'react';

import { useSelector, useDispatch } from 'react-redux';
import { fetchVideos, fetchVideoInfo } from './redux/actions';

import VideoInfo from './components/VideoInfo';
import ControlPanel from './components/ControlPanel';
import PreviewPanel from './components/PreviewPanel';
import Slider from './components/Slider';

export default () => {

    const dispatch = useDispatch();

    const backend_api = useSelector(state => state.app.backend_api);
    const videosList = useSelector(state => state.player.videosList);
    const videoInfo = useSelector(state => state.player.videoInfo);


    useEffect(() => {
        if (!videosList.length) {
            dispatch(fetchVideos(backend_api + 'get_videos_list/'));
        }

        if (videosList.length && !Object.keys(videoInfo).length) {
            let url = backend_api + 'get_video_info?filename=' + videosList[0];
            url = encodeURI(url);

            dispatch(fetchVideoInfo(url));
        }
    })

    if (Object.keys(videoInfo).length) {
        return (
            <div className="page__content">
                <VideoInfo />
                <ControlPanel />
                <PreviewPanel />
                <Slider />
            </div>
        )
    }

    // return 'Loading...';
    return '';
}
