from pathlib import Path
from django.conf import settings
from django.http import JsonResponse, HttpResponse, HttpResponseNotFound

from video_utils.image_extractor import video_info, get_binary_color_image, get_binary_IR_image, get_binary_depth_image, get_binary_transformed_color_image, get_binary_transformed_depth_image


def test(request):
    data = {'Greeting': 'Hello from video_viewer_api!'}
    return JsonResponse(data)


def get_videos_list(request):
    list = []

    for item in settings.VIDEO_PATH.iterdir():
        if item.match('*.mkv') or item.match('*.MKV'):
            list.append(item.name)

    return JsonResponse(list, safe=False)


def get_video_info(request):
    path = settings.VIDEO_PATH / request.GET.get('filename', '')
    info = video_info(path)
    return JsonResponse(info)


def get_image(request):
    path = settings.VIDEO_PATH / request.GET.get('filename', '')
    image_type = request.GET.get('image_type', 'color')
    image_space = request.GET.get('image_space', 'color')
    time = float(request.GET.get('time', 0.0))

    data = None

    if (image_type == 'color' and image_space == 'color'):
        data = get_binary_color_image(path, time)
    elif (image_type == 'color' and image_space == 'depth'):
        data = get_binary_transformed_color_image(path, time)
    elif (image_type == 'depth' and image_space == 'depth'):
        data = get_binary_depth_image(path, time)
    elif (image_type == 'depth' and image_space == 'color'):
        data = get_binary_transformed_depth_image(path, time)
    elif (image_type == 'ir'):
        data = get_binary_IR_image(path, time)

    if data is None:
        return HttpResponseNotFound()

    return HttpResponse(data, content_type='image/png')
