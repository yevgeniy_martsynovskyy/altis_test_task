from django.apps import AppConfig


class VideoViewerApiConfig(AppConfig):
    name = 'video_viewer_api'
