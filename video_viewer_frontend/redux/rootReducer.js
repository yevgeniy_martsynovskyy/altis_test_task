import { combineReducers } from 'redux';
import { playerReducer } from './playerReducer';
import { appReducer } from './appReducer';

export const rootReducer = combineReducers({
    app: appReducer,
    player: playerReducer
});
