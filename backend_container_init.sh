#!/bin/sh

set -e

pid=0

# signal handler
term_int_handler() {
    if [ $pid -ne 0 ]; then
        kill -TERM "$pid"
        wait "$pid"
    fi
    echo -e "********************************"
    echo -e "TERMINATION of pid ${pid}\n"

    exit 143; # 128 + 15 -- SIGTERM
}

# setup handlers
trap 'kill ${!}; term_int_handler' INT TERM

# check for database presence 
if [ ! -e /server/databases/db.sqlite3 ]; then
    # create empty database and apply django migrations
    python3 manage.py migrate
    # create new superuser with admin/admin credentials
    python3 manage.py shell << _EOF_
from django.contrib.auth.models import User;
User.objects.create_superuser('admin', 'admin@admin.com', 'admin')
_EOF_
    echo -e "********** ATTENTION! **********"
    echo -e "Database is unavailable"
    echo -e "New empty database was created. Superuser is admin/admin"
    echo -e "********************************"
fi

# check django applications and copy static files
python3 manage.py check && python3 manage.py collectstatic --noinput
echo -e "********************************"
echo -e "Static files COLLECTED"

# run application
uwsgi --ini backend_uwsgi.ini &
pid="$!"
echo -e "********************************"
echo -e "UWSGI server LAUNCHED"

tail -f /dev/null & wait ${!}
