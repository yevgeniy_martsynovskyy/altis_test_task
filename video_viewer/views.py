from django.http import HttpResponse
from django.views.generic import TemplateView

def test(request):
    return HttpResponse("Hello, world. You're at the polls index.")

class IndexView(TemplateView):
    template_name = 'video_viewer/index.html'
