export const FETCH_VIDEOS = 'PLAYER/FETCH_VIDEOS';
export const FETCH_VIDEO_INFO = 'PLAYER/FETCH_VIDEO_INFO';
export const SET_TIME = 'PLAYER/SET_TIME';
export const SET_IMAGE_TYPE = 'PLAYER/SET_IMAGE_TYPE';
export const SET_IMAGE_SPACE = 'PLAYER/SET_IMAGE_SPACE';
