import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { setTime } from '../redux/actions';
import { throttle } from '../utils';

export default () => {
    const DELAY = 500 // milliseconds

    const videoInfo = useSelector(state => state.player.videoInfo);
    const dispatch = useDispatch();

    const changeHandler = event => {
        event.preventDefault();
        dispatch(setTime(event.target.value));
    }

    return (
        <div className="page__element slider">
            <input type="range" className="slider__inner"
                min="0" max={videoInfo.duration} step="0.033"
                defaultValue="0"
                onChange={throttle(changeHandler, DELAY)}
            />
        </div>
    )
}
