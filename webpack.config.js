const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const BundleTracker = require('webpack-bundle-tracker');

const filename = ext => `[name]/[name].[fullhash:8].${ext}`;

module.exports = {
    context: path.resolve(__dirname),
    mode: 'development',
    entry: {
        video_viewer_frontend: './video_viewer_frontend/index.js'
    },
    output: {
        filename: filename('js'),
        path: path.resolve(__dirname, 'webpack_bundles/'),
        publicPath: ''
    },
    resolve: {
        extensions: ['.js', '.jsx'],
        modules: ['./node_modules']
    },
    plugins: [
        new MiniCssExtractPlugin({ filename: filename('css') }),
        new BundleTracker({
            path: path.resolve(__dirname, 'webpack_bundles/'),
            filename: 'webpack-stats.json',
            indent: 2
        }),
        new CleanWebpackPlugin({ verbose: true })
    ],
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                ]
            },
            {
                test: /\.(js|jsx)$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader',
                options: {
                    presets: [
                        ['@babel/preset-env', {
                            targets: 'last 2 Chrome versions',
                            useBuiltIns: 'usage',
                            corejs: { version: 3, proposals: true }
                        }],
                        ['@babel/preset-react']
                    ]
                }
            }
        ]
    }
};
