from django.urls import path

from . import views

app_name = 'video_viewer'

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('test/', views.test, name='test'),
]
