import {
    FETCH_VIDEOS, FETCH_VIDEO_INFO, SET_TIME,
    SET_IMAGE_TYPE, SET_IMAGE_SPACE, SET_IMAGE_TYPE_AND_SPACE
} from './types';

const initialState = {
    videosList: [],
    videoInfo: {},
    imageType: 'color',
    imageSpace: { 'color': 'color', 'ir': 'ir', 'depth': 'depth' },
    time: 0,
};

export const playerReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_VIDEOS:
            return { ...state, videosList: action.payload };
        case FETCH_VIDEO_INFO:
            return { ...state, videoInfo: action.payload };
        case SET_TIME:
            return { ...state, time: action.payload };
        case SET_IMAGE_TYPE:
            return { ...state, imageType: action.payload };
        case SET_IMAGE_SPACE:
            const newState = { ...state };
            newState.imageSpace[newState.imageType] = action.payload;
            return newState;
        default: return state;
    }
}
