import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, compose, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';

import './styles/styles.css';

import App from './App';
import { rootReducer } from './redux/rootReducer';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(rootReducer, composeEnhancers(
    applyMiddleware(thunk),
    // window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
));

const app = React.createElement(
    Provider,
    { store: store },
    React.createElement(App)
)

/* const app = (
    <Provider store={store}>
        <App />
    </Provider>
) */

ReactDOM.render(
    app,
    document.getElementById('root')
);
