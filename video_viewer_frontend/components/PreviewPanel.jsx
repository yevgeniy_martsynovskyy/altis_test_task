import React from 'react';
import { useSelector } from 'react-redux';

export default () => {
    const backend_api = useSelector(state => state.app.backend_api);
    const videoInfo = useSelector(state => state.player.videoInfo);
    const time = useSelector(state => state.player.time);
    const imageType = useSelector(state => state.player.imageType);
    const imageSpace = useSelector(state => state.player.imageSpace[imageType]);

    const errorHandler = (e) => {
        // console.log(e);
        e.target.src = 'https://www.publicdomainpictures.net/pictures/280000/velka/not-found-image-15383864787lu.jpg';
    }

    let src = backend_api + 'get_image/?filename=' + videoInfo.name +
        '&image_type=' + imageType + '&image_space=' + imageSpace + '&time=' + time;
    src = encodeURI(src);

    console.log(src);

    return (
        <div className="page__element page__element_wide preview-panel">
            <img className="preview-panel__image" src={src} alt="Image not found"
                onError={errorHandler} />
        </div>
    )
}
