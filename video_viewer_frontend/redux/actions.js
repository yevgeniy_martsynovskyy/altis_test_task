import {
    FETCH_VIDEOS, FETCH_VIDEO_INFO, SET_TIME,
    SET_IMAGE_TYPE, SET_IMAGE_SPACE
} from './types';

export function fetchVideos(url) {
    return async dispatch => {
        try {
            const response = await fetch(url);
            const json = await response.json();
            dispatch({ type: FETCH_VIDEOS, payload: json });
        } catch (e) {
            console.log(e);
        }
    }
}

export function fetchVideoInfo(url) {
    return async dispatch => {
        try {
            const response = await fetch(url);
            const json = await response.json();
            dispatch({ type: FETCH_VIDEO_INFO, payload: json });
        } catch (e) {
            console.log(e);
        }
    }
}

export function setTime(time) {
    return {
        type: SET_TIME,
        payload: time
    }
}

export function setImageType(type) {
    return {
        type: SET_IMAGE_TYPE,
        payload: type
    }
}

export function setImageSpace(targetSpace) {
    return {
        type: SET_IMAGE_SPACE,
        payload: targetSpace
    }
}
