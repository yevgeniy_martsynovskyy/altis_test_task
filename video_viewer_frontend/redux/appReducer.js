const initialState = {
    backend_api: '/video_viewer_api/'
}

export const appReducer = (state = initialState, action) => {
    switch (action.type) {
        default: return state;
    }
}
