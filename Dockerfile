FROM node:12-alpine AS webpack-builder

COPY package.json /tmp/package.json
RUN cd /tmp && npm install
RUN mkdir -p /build_area/ && cp -a /tmp/node_modules/ /tmp/package-lock.json /tmp/package.json /build_area/

WORKDIR /build_area/

COPY video_viewer_frontend video_viewer_frontend/
COPY webpack.config.js .

RUN npm run build

### SECOND STAGE ###

FROM python:3

WORKDIR /server/

RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add - && \
    echo "deb https://packages.microsoft.com/ubuntu/18.04/prod bionic main" > /etc/apt/sources.list.d/microsoft.list && \
    apt-get update -q -y && \
    env DEBIAN_FRONTEND=noninteractive ACCEPT_EULA=Y apt-get install -q -y libk4a1.4-dev

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

# Copy django and uwsgi config
COPY django_backend django_backend/
COPY manage.py .
COPY backend_uwsgi.ini .

# Copy applications
COPY video_utils video_utils/
COPY video_viewer video_viewer/
COPY video_viewer_api video_viewer_api/

# Copy web-assets from previous stage
COPY --from=webpack-builder /build_area/webpack_bundles webpack_bundles/

# Create databases directory
# RUN mkdir -p ./databases/

# Launch script
COPY backend_container_init.sh .
CMD ["/server/backend_container_init.sh"]
