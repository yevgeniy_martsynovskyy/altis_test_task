import React, { useEffect } from 'react';

import { useSelector, useDispatch } from 'react-redux';

import VideoSelector from './VideoSelector';

export default () => {
    const BYTES_IN_MEGABYTE = 1e6;

    const videoInfo = useSelector(state => state.player.videoInfo);

    const makeListItem = (name, value) => {
        return (
            <li className="list__item" key={name}>
                <span className="list__item-name">{name}</span>
                <span className="list__item-value">{value}</span>
            </li>
        )
    }

    return (
        <div className="video-info page__element">
            <h2 className="video-info__title">{videoInfo.name}</h2>
            <div className="video-info__lists">
                <ul className="list">
                    {makeListItem('Color resolution:', videoInfo.colorResolution)}
                    {makeListItem('Depth resolution:', videoInfo.depthResolution)}
                    {makeListItem('IR resolution:', videoInfo.irResolution)}
                </ul>
                <ul className="list">
                    {makeListItem('Size:', (videoInfo.size / BYTES_IN_MEGABYTE).toFixed(2) + ' MB')}
                    {makeListItem('Duration:', videoInfo.duration.toFixed(2) + ' sec')}
                </ul>
            </div>
            <div className="video-info__selector">
                <VideoSelector />
            </div>
        </div>

    )
}
