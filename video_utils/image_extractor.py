from pathlib import Path
import cv2
from pyk4a import PyK4APlayback
from pyk4a.transformation import color_image_to_depth_camera, depth_image_to_color_camera

from .helpers import convert_to_bgra_if_required, colorize

MICROSECONDS_IN_SECOND = 1E6
UPPER_LIMIT_IR = 5000
UPPER_LIMIT_DEPTH = 2500


def video_info(path):
    info = dict()

    p = Path(path)
    info['name'] = p.name
    info['size'] = p.stat().st_size

    playback = PyK4APlayback(path)
    playback.open()

    try:
        info['duration'] = float(playback.length / MICROSECONDS_IN_SECOND)

        playback.seek(int(playback.length / 2))
        capture = playback.get_next_capture()

        if capture.color is not None:
            rgb_image = cv2.imdecode(capture.color, cv2.IMREAD_UNCHANGED)
            info['colorResolution'] = str(rgb_image.shape[1]) + ' x ' + str(rgb_image.shape[0])
        else:
            info['colorResolution'] = 'N/A'

        if capture.ir is not None:
            info['irResolution'] = str(capture.ir.shape[1]) + ' x ' + str(capture.ir.shape[0])
        else:
            info['irResolution'] = 'N/A'

        if capture.depth is not None:
            info['depthResolution'] = str(capture.depth.shape[1]) + \
                ' x ' + str(capture.depth.shape[0])
        else:
            info['depthResolution'] = 'N/A'

    finally:
        playback.close()

    return info


def get_binary_color_image(path, offset):
    image_bytes = None

    playback = PyK4APlayback(path)
    playback.open()

    try:
        if offset == 0:
            playback.seek(playback.configuration['start_timestamp_offset_usec'])
        elif offset > 0:
            playback.seek(int(offset * MICROSECONDS_IN_SECOND))

        capture = playback.get_next_capture()

        if capture.color is not None:
            converted_image = convert_to_bgra_if_required(
                playback.configuration["color_format"], capture.color)
            _, image_array = cv2.imencode('frame.png', converted_image)
            image_bytes = image_array.tobytes()
    finally:
        playback.close()
        return image_bytes


def get_binary_IR_image(path, offset):
    image_bytes = None

    playback = PyK4APlayback(path)
    playback.open()

    try:
        if offset > 0:
            playback.seek(int(offset * MICROSECONDS_IN_SECOND))

        capture = playback.get_next_capture()

        if capture.ir is not None:
            colorized_ir = colorize(capture.ir,
                                    clipping_range=(0, UPPER_LIMIT_IR),
                                    colormap=cv2.COLORMAP_PINK)
            _, image_array = cv2.imencode('frame.png', colorized_ir)
            image_bytes = image_array.tobytes()
    finally:
        playback.close()
        return image_bytes


def get_binary_depth_image(path, offset):
    image_bytes = None

    playback = PyK4APlayback(path)
    playback.open()

    try:
        if offset > 0:
            playback.seek(int(offset * MICROSECONDS_IN_SECOND))

        capture = playback.get_next_capture()

        if capture.depth is not None:
            colorized_depth = colorize(capture.depth,
                                       clipping_range=(0, UPPER_LIMIT_DEPTH),
                                       colormap=cv2.COLORMAP_RAINBOW)
            _, image_array = cv2.imencode('frame.png', colorized_depth)
            image_bytes = image_array.tobytes()
    finally:
        playback.close()
        return image_bytes


def get_binary_transformed_color_image(path, offset):
    image_bytes = None

    playback = PyK4APlayback(path)
    playback.open()

    try:
        if offset == 0:
            playback.seek(playback.configuration['start_timestamp_offset_usec'])
        elif offset > 0:
            playback.seek(int(offset * MICROSECONDS_IN_SECOND))

        capture = playback.get_next_capture()

        if capture.color is not None:
            rgb_image = cv2.imdecode(capture.color, cv2.IMREAD_UNCHANGED)
            rgba_image = cv2.cvtColor(rgb_image, cv2.COLOR_RGB2RGBA)
            transformed_image = color_image_to_depth_camera(
                rgba_image, capture.depth, playback.calibration, capture.thread_safe)
            _, image_array = cv2.imencode('frame.png', transformed_image)
            image_bytes = image_array.tobytes()
    finally:
        playback.close()
        return image_bytes


def get_binary_transformed_depth_image(path, offset):
    image_bytes = None

    playback = PyK4APlayback(path)
    playback.open()

    try:
        if offset > 0:
            playback.seek(int(offset * MICROSECONDS_IN_SECOND))

        capture = playback.get_next_capture()

        if capture.depth is not None:
            transformed_depth = depth_image_to_color_camera(
                capture.depth, playback.calibration, capture.thread_safe)
            colorized_transformed_depth = colorize(transformed_depth, clipping_range=(
                0, UPPER_LIMIT_DEPTH), colormap=cv2.COLORMAP_RAINBOW)
            _, image_array = cv2.imencode('frame.png', colorized_transformed_depth)
            image_bytes = image_array.tobytes()
    finally:
        playback.close()
        return image_bytes
