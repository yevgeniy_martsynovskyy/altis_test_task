import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { setImageType } from '../redux/actions';

export default () => {

    const dispatch = useDispatch();
    const imageType = useSelector(state => state.player.imageType);

    const changeHandler = event => {
        dispatch(setImageType(event.target.value));
    }

    const makeButtons = () => {
        const buttons = {
            'color': 'Color',
            'ir': 'IR',
            'depth': 'Depth'
        }

        return Object.keys(buttons).map(key => {
            const defaultClass = 'image-type-switcher__button';
            const selectedClass = defaultClass + ' image-type-switcher__button_selected';
            const classNames = imageType === key ? selectedClass : defaultClass;

            return (
                <label className={classNames} key={key}>
                    {buttons[key]}
                    <input className="image-type-switcher__radio"
                        type="radio" name="image_type" value={key}
                        onChange={changeHandler} />
                </label>
            )
        })
    }

    return (
        <div className="image-type-switcher">
            {makeButtons()}
        </div>
    )
}
