import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { setImageSpace } from '../redux/actions';

export default () => {

    const dispatch = useDispatch();
    const imageType = useSelector(state => state.player.imageType);
    const imageSpace = useSelector(state => state.player.imageSpace[imageType]);

    const changeHandler = event => {
        dispatch(setImageSpace(event.target.value));
    }

    const makeOptions = () => {

        const options = {};
        switch (imageType) {
            case 'color':
            case 'depth':
                options['color'] = 'Color';
                options['depth'] = 'Depth';
                break;
            case 'ir':
                options['ir'] = 'IR';
                break;
            default:
                options['color'] = 'Color';
                options['depth'] = 'Depth';
        }


        return Object.keys(options).map(key => {
            const defaultClass = 'image-space-switcher__radio';
            const selectedClass = defaultClass + ' image-space-switcher__radio_selected';
            const classNames = imageSpace === key ? selectedClass : defaultClass;

            return (
                <label className="image-space-switcher__label" key={imageType + key}>
                    <input className={classNames}
                        type="radio" name="image_space" value={key}
                        onChange={changeHandler} />
                    {options[key]}
                </label>
            )
        })
    }

    return (
        <div className="image-space-switcher">
            <span className="image-space-switcher__text">Image space:</span>
            {makeOptions()}
        </div>
    )
}
